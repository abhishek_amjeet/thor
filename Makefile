SHELL = /bin/bash
WEB_PORT ?= "3030"
ARTIFACTORY_REPO ?= "abmitra2017"
IMAGE_VERSION ?= "0.0.1"
REPO = $(shell go list -m)


.PHONY: build
build: ## builds the application in acontainer
	docker pull golang:1.16
	docker pull alpine:latest
	docker build -f docker/Dockerfile . --build-arg WEB_PORT_VALUE=$(WEB_PORT) --tag  $(ARTIFACTORY_REPO)/$(REPO):$(IMAGE_VERSION)

.PHONY: push
push: build ## pushes the container to dockerhub
	docker push  $(ARTIFACTORY_REPO)/$(REPO):$(IMAGE_VERSION)


.DEFAULT_GOAL := help
.PHONY: help
help: ## Show this help screen.
	@echo 'Usage: make <OPTIONS> ... <TARGETS>'
	@echo ''
	@echo 'Available targets are:'
	@echo ''
	@awk 'BEGIN {FS = ":.*##"; printf "\nUsage:\n  make \033[36m<target>\033[0m\n"} /^[a-zA-Z0-9_-]+:.*?##/ { printf "  \033[36m%-25s\033[0m %s\n", $$1, $$2 } /^##@/ { printf "\n\033[1m%s\033[0m\n", substr($$0, 5) } ' $(MAKEFILE_LIST)