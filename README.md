# thor

Simple REST-API service with in memory key value cache. 

The service supports the following URls

```
Name == > Health Endpoint : Route ==> /health: Methods = [GET]
Name == > Get Key : Route ==> /Get: Methods = [GET]
Name == > Set Key : Route ==> /Set: Methods = [GET]
Name == > Delete Key : Route ==> /Delete: Methods = [GET]
Name == > Delete Cache : Route ==> /Flush: Methods = [GET]
Name == > Memory Dump : Route ==> /Dump: Methods = [GET]
Name == > Memory Dump : Route ==> /Metrics: Methods = [GET]
```

## Building the application

```
export WEB_PORT=8119 # example port number
export ARTIFACTORY_REPO=<dockerhub-user-name> 
export IMAGE_VERSION=1.0.0 
make push
```

## Running the application

### Running the application locally on MAC

```
docker run -p 8119:8119 docker.io/<dockerhub-user-name>/thor:1.0.0
```

. Sample Output
```
Name == > Health Endpoint : Route ==> /health: Methods = [GET]
Name == > Get Key : Route ==> /Get: Methods = [GET]
Name == > Set Key : Route ==> /Set: Methods = [GET]
Name == > Delete Key : Route ==> /Delete: Methods = [GET]
Name == > Delete Cache : Route ==> /Flush: Methods = [GET]
Name == > Memory Dump : Route ==> /Dump: Methods = [GET]
Name == > Memory Dump : Route ==> /Metrics: Methods = [GET]
{"level":"INFO","time":"2022-03-19T20:08:00.759Z","caller":"router/mux.go:48","message":"Starting web server at http://0.0.0.0:8119"}
```

## Example Usage 

#### Populate Data with `/Set`

Input 
```
curl 'http://0.0.0.0:8119/Set?key=Hello&value=Bob'
```

Output 
```json
 {
   "response": [
      {
         "key": "Hello",
         "value": "Bob",
         "status": true
      }
   ],
   "operation": "Set",
   "dbDump": null,
   "timeStamp": "2022-03-19T22:57:56.127122Z"
}
```

#### Retrieve Data with `/Get` 

Input
```
curl 'http://0.0.0.0:8119/Get?key=Hello'
```

Output
```json
 {
   "response": [
      {
         "key": "Hello",
         "value": "Bob",
         "status": true
      }
   ],
   "operation": "Get",
   "dbDump": null,
   "timeStamp": "2022-03-19T22:58:48.968948Z"
}
```
#### Delete a key with `/Delete`


Input
```
curl 'http://0.0.0.0:8119/Delete?key=Hello'
```

Output
```json
 {
   "response": [
      {
         "key": "Hello",
         "value": "",
         "status": true
      }
   ],
   "operation": "Delete",
   "dbDump": null,
   "timeStamp": "2022-03-19T23:00:14.913554Z"
}
```


#### Set multiple values with a single call 

Input 

```
curl 'http://0.0.0.0:8119/Set?key=Dusty&value=Bobcat&key=Happy&value=Feet&key=Severus&value=Snape'
```

Output

```json
{
 "response": [
  {
   "key": "Dusty",
   "value": "Bobcat",
   "status": true
  },
  {
   "key": "Happy",
   "value": "Feet",
   "status": true
  },
  {
   "key": "Severus",
   "value": "Snape",
   "status": true
  }
 ],
 "operation": "Set",
 "dbDump": null,
 "timeStamp": "2022-03-19T23:05:40.24251Z"
}
```

#### Get multiple values with a single call

Input 
```
curl 'http://0.0.0.0:8119/Get?key=Dusty&key=Happy'
```

Output
```json
{
 "response": [
  {
   "key": "Dusty",
   "value": "Bobcat",
   "status": true
  },
  {
   "key": "Happy",
   "value": "Feet",
   "status": true
  }
 ],
 "operation": "Get",
 "dbDump": null,
 "timeStamp": "2022-03-19T23:06:37.765273Z"
}
```

#### Display all data in memory with `/Dump`

Input
```
curl 'http://0.0.0.0:8119/Dump'
```

Output
```json
 {
   "response": null,
   "operation": "Dump",
   "dbDump": {
      "Albus": "Dumbeldore",
      "Dusty": "Bobcat",
      "Happy": "Feet",
      "Julius": "Caesar",
      "Robert": "Langdon",
      "Severus": "Snape"
   },
   "timeStamp": "2022-03-19T23:08:03.663799Z"
}
```

#### Display stats with `/Metrics`

Input
```
curl 'http://0.0.0.0:8119/Metrics'
```

Output
```json
{
   "key_count": 6,
   "data_size": 96,
   "hit_count": 7
}
```

#### Delete a single key with `/Delete`

Input
```
curl 'http://0.0.0.0:8119/Delete?key=Happy' 
```

Output
```json
{
   "response": [
      {
         "key": "Happy",
         "value": "",
         "status": true
      }
   ],
   "operation": "Delete",
   "dbDump": null,
   "timeStamp": "2022-03-19T23:09:44.27001Z"
}
```

#### Delete multiple keys with `/Delete`

```
curl 'http://0.0.0.0:8119/Dump
```
```json
{
 "response": null,
 "operation": "Dump",
 "dbDump": {
  "Albus": "Dumbeldore",
  "Dusty": "Bobcat",
  "Julius": "Caesar",
  "Robert": "Langdon",
  "Severus": "Snape"
 },
 "timeStamp": "2022-03-19T23:10:47.185778Z"
}
```
```
curl 'http://0.0.0.0:8119/Delete?key=Julius&key=Albus'
```
```json
{
 "response": [
  {
   "key": "Julius",
   "value": "",
   "status": true
  },
  {
   "key": "Albus",
   "value": "",
   "status": true
  }
 ],
 "operation": "Delete",
 "dbDump": null,
 "timeStamp": "2022-03-19T23:11:00.731435Z"
}
```
```
curl 'http://0.0.0.0:8119/Dump
```
```json
{
 "response": null,
 "operation": "Dump",
 "dbDump": {
  "Dusty": "Bobcat",
  "Robert": "Langdon",
  "Severus": "Snape"
 },
 "timeStamp": "2022-03-19T23:11:51.666681Z"
}
```
#### Drop the DB to start over with `/Flush`

```
curl 'http://0.0.0.0:8119/Flush'
```
```json

{
 "response": null,
 "operation": "Flush",
 "dbDump": null,
 "timeStamp": "2022-03-19T23:12:48.983493Z"
}
```
```
curl 'http://0.0.0.0:8119/Dump'
```
```json
{
 "response": null,
 "operation": "Dump",
 "dbDump": {},
 "timeStamp": "2022-03-19T23:12:53.373074Z"
}
```
