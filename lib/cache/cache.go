package cache

// This is the core of the thor-db application
// It is a simple in-memory cache implemented in go-lang
// It uses the sync package which uses locks to prevent race conditions

import (
	"go.uber.org/zap"
	"reflect"
	"sync"
)

var (
	once              sync.Once
	MemcCacheInstance MemCache
)

type MemCache struct {
	*memCache
}

type memCache struct {
	items map[string]interface{}
	mutex sync.Mutex
}

// constructor method. This creates a new cache when invoked
// Its executed when the mux is started
// Each of the Set/Get and Delete calls aquire a lock to avoid race conditions
func New() MemCache {
	once.Do(func() {
		c := &memCache{
			items: make(map[string]interface{}),
		}
		MemcCacheInstance = MemCache{c}
	})
	return MemcCacheInstance
}

func (c *memCache) Set(key string, value interface{}) error {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	c.items[key] = value
	zap.S().Infof("Key '%s' stored in cache\n", key)
	return nil
}

func (c *memCache) Get(key string) interface{} {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	val, ok := c.items[key]
	if !ok {
		zap.S().Errorf("Cache miss for key %s", key)
		return nil
	}

	zap.S().Infof("Key '%s' found", key)
	return val
}

func (c *memCache) Delete(key string) bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	_, ok := c.items[key]
	if ok {
		delete(c.items, key)
		zap.S().Infof("key %s cleaned up", key)
		return true
	}

	zap.S().Errorf("Cache miss for key %s", key)
	return false
}

func (c *memCache) DeleteAll() bool {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	for k := range c.items {
		delete(c.items, k)
	}
	return true
}

func (c *memCache) GetAll() map[string]interface{} {
	c.mutex.Lock()
	defer c.mutex.Unlock()
	return c.items
}

func (c *memCache) GetMetrics() (int, int) {
	c.mutex.Lock()
	defer c.mutex.Unlock()

	var totalSize int
	for k := range c.items {
		totalSize = totalSize + int(reflect.TypeOf(c.items[k]).Size())
	}
	return len(c.items), totalSize
}
