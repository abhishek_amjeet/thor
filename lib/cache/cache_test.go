package cache

import "testing"

func TestCacheGet(t *testing.T) {
	tests := []struct {
		name    string
		key     string
		value   string
		wantErr bool
	}{
		{
			name:    "Cache set",
			key:     "foo",
			value:   "bar",
			wantErr: false,
		},
		{
			name:    "Cache set concat",
			key:     "foo",
			value:   "bar" + "world",
			wantErr: false,
		},
		{
			name:    "Cache hit with get",
			key:     "foo",
			value:   "bar",
			wantErr: false,
		},
		{
			name:    "Cache miss with get",
			key:     "foo",
			value:   "bar",
			wantErr: true,
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cd := New()
			cd.Set(tt.key, tt.value)
			if cd.Get(tt.key) != tt.value && tt.wantErr {
				t.Errorf("Test Failed")
			}
		})
	}
}

func TestCacheDeleteSucess(t *testing.T) {
	tests := []struct {
		name  string
		key   string
		value string
	}{
		{
			name:  "Value deleted",
			key:   "foo",
			value: "bar",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cd := New()
			cd.Set(tt.key, tt.value)
			if !cd.Delete(tt.key) {
				t.Errorf("Test Failed")
			}
		})
	}
}
func TestCacheDeleteFailed(t *testing.T) {
	tests := []struct {
		name  string
		key   string
		value string
	}{
		{
			name:  "Value not deleted",
			key:   "foo",
			value: "bar",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cd := New()
			cd.Set(tt.key, tt.value)
			if cd.Delete("bar") {
				t.Errorf("Test Failed")
			}

		})
	}
}

func TestCacheDeleteAll(t *testing.T) {
	tests := []struct {
		name  string
		key   string
		value string
	}{
		{
			name:  "Value added",
			key:   "foo",
			value: "bar",
		},
		{
			name:  "Value added",
			key:   "foo1",
			value: "bar1",
		},
		{
			name:  "Value added",
			key:   "foo2",
			value: "bar2",
		},
	}

	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			cd := New()
			cd.Set(tt.key, tt.value)
			if !cd.DeleteAll() {
				t.Errorf("Test Failed")
			}
		})
	}
}

func TestGetAll(t *testing.T) {
	testKey := "test"
	testValue := "test value"
	cd := New()
	cd.Set(testKey, testValue)
	if cd.GetAll() == nil {
		t.Errorf("TestGetAll failed")
	}

}
