package constants

import "time"

const (
	SERVER_ADDRESS    = "0.0.0.0"
	WEB_PORT_KEY      = "WEB_PORT"
	DEFAULT_PORT_NUM  = "3000"
	ConextTimeOut     = time.Second * 30
	SET_OPERATION     = "Set"
	GET_OPERATION     = "Get"
	DELETE_OPERATION  = "Delete"
	FLUSH_OPERATION   = "Flush"
	METRICS_OPERATION = "Metrics"
	DUMP_OPERATION    = "Dump"
	DEV_TEST          = "dev"
	TrueValue         = "True"
	FalseValue        = "False"
)
