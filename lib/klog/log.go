package klog

import (
	"context"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
	"log"
)

// Initializing the Zap logger
// Is used by middleware for logging
func Logger(ctx context.Context) *zap.Logger {
	logger, err := zap.NewProduction()
	if err != nil {
		log.Println("Unable to initialize klog")
	}
	defer logger.Sync()

	cfg := zap.Config{
		Encoding:         "json",
		Level:            zap.NewAtomicLevelAt(zapcore.DebugLevel),
		OutputPaths:      []string{"stderr"},
		ErrorOutputPaths: []string{"stderr"},
		EncoderConfig: zapcore.EncoderConfig{
			MessageKey:  "message",
			LevelKey:    "level",
			EncodeLevel: zapcore.CapitalLevelEncoder,

			TimeKey:    "time",
			EncodeTime: zapcore.ISO8601TimeEncoder,

			CallerKey:    "caller",
			EncodeCaller: zapcore.ShortCallerEncoder,
		},
	}
	logger, err = cfg.Build()
	if err != nil {
		logger.Sugar().Errorf("Unable to build logger due to %v", err)
	}
	return logger
}
