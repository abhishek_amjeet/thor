package middleware

import (
	"context"
	"net/http"
	"thor/lib/constants"
	"thor/lib/klog"
)

// Simple middleware to log messages for every request
func LoggingMiddleware(next http.Handler) http.Handler {
	return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
		ctx, cancel := context.WithTimeout(context.Background(), constants.ConextTimeOut)
		defer cancel()
		logger := klog.Logger(ctx)
		logger.Sugar().Infof("Request URI => %v", r.RequestURI)
		next.ServeHTTP(w, r)
	})
}
