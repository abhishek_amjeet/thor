package router

import (
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"net/http"
	"net/http/httptest"
	"net/url"
	"os"
	"strings"
	"testing"
	"thor/lib/constants"
)

// TestHealthHandler tests health endpoint for thor-db
func TestHealthHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/health", nil)
	if err != nil {
		t.Fatal(err)
	}
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(Health)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}

	// Check the response body is what we expect.
	expected := map[string]bool{"ok": true}
	bdata, _ := json.Marshal(expected)
	if err != nil {
		fmt.Println("marhsalling error", zap.Error(err))
	}
	if rr.Body.String() != string(bdata) {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

// TestCacheInsertHandler tests Set endpoint for thor-db
func TestCacheInsertHandler(t *testing.T) {
	data := url.Values{}
	data.Set("key", "foo")
	data.Set("value", "bar")
	req, err := http.NewRequest("GET", "/Set", strings.NewReader(data.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(CacheInsert)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

// TestCacheGetHandler tests Get endpoint for thor-db
func TestCacheGetHandler(t *testing.T) {
	data := url.Values{}
	data.Set("key", "foo")
	req, err := http.NewRequest("GET", "/Get", strings.NewReader(data.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(CacheGet)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

// TestCacheDeleteHandler tests Delete endpoint for thor-db
func TestCacheDeleteHandler(t *testing.T) {
	data := url.Values{}
	data.Set("key", "foo")
	req, err := http.NewRequest("GET", "/Delete", strings.NewReader(data.Encode()))
	if err != nil {
		t.Fatal(err)
	}

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(CacheDelete)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

// TestCacheFlushHandler tests Flush endpoint for thor-db
func TestCacheFlushHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/Flush", nil)
	if err != nil {
		t.Fatal(err)
	}
	os.Setenv(constants.DEV_TEST, constants.TrueValue)
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(CacheFlush)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	os.Unsetenv(constants.DEV_TEST)
}

// TestCacheMetricsHandler tests Metrics endpoint for thor-db
func TestCacheMetricsHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/Metrics", nil)
	if err != nil {
		t.Fatal(err)
	}
	os.Setenv(constants.DEV_TEST, constants.TrueValue)

	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(CacheMetrics)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	os.Unsetenv(constants.DEV_TEST)
}

// TestCacheDumpHandler tests Get All/ Dump endpoint for thor-db
func TestCacheDumpHandler(t *testing.T) {
	req, err := http.NewRequest("GET", "/Dump", nil)
	if err != nil {
		t.Fatal(err)
	}

	os.Setenv(constants.DEV_TEST, constants.TrueValue)
	rr := httptest.NewRecorder()

	handler := http.HandlerFunc(CacheDump)
	handler.ServeHTTP(rr, req)

	// Check the status code is what we expect.
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	os.Unsetenv(constants.DEV_TEST)
}
