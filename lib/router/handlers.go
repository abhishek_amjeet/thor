package router

import (
	"context"
	"encoding/json"
	"fmt"
	"go.uber.org/zap"
	"math/rand"
	"net/http"
	"thor/lib/constants"
	"thor/lib/klog"
	"time"
)

var (
	HitCount int
)

// Health endpoint for checking the mux status
func Health(w http.ResponseWriter, r *http.Request) {
	ctx, cancel := context.WithTimeout(context.Background(), constants.ConextTimeOut)
	defer cancel()
	logger := klog.Logger(ctx)
	data := map[string]bool{"ok": true}
	bdata, err := json.Marshal(data)
	if err != nil {
		logger.Sugar().Errorf("marshalling error", zap.Error(err))
	}
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	w.Write(bdata)
}

// CacheGet will get the values of a single or multiple keys stored in memory
// and return a json formatted response
func CacheGet(w http.ResponseWriter, r *http.Request) {
	HitCount = HitCount + 1
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	var results CacheResponses
	var result CacheResponse
	keys := r.URL.Query()["key"]
	for _, k := range keys {
		result.Key = k
		cache_data := Data.Get(k)
		result.Status = false
		result.Value = "VALUE NOT FOUND"
		if cache_data != nil {
			result.Status = true
			result.Value = fmt.Sprintf("%v", cache_data)
		}

		results.Response = append(results.Response, result)
	}
	results.Operation = constants.GET_OPERATION
	results.TimeStamp = time.Now().UTC()
	byteData, err := json.MarshalIndent(results, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)
}

// CacheInsert will insert the values of a single or multiple keys from input request
// and return a json formatted response to signal success / failure
func CacheInsert(w http.ResponseWriter, r *http.Request) {
	HitCount = HitCount + 1
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	keys := r.URL.Query()["key"]
	values := r.URL.Query()["value"]
	if len(keys) != len(values) {
		fmt.Fprintln(w, "Number of keys and values are not the same")
	}
	var results CacheResponses
	var result CacheResponse
	for i, k := range keys {
		result.Key = k
		result.Value = values[i]
		if Data.Set(k, values[i]) == nil {
			result.Status = true
		}
		results.Response = append(results.Response, result)
	}
	results.Operation = constants.SET_OPERATION
	results.TimeStamp = time.Now().UTC()
	byteData, err := json.MarshalIndent(results, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)
}

// CacheDelete will delete the values of a single or multiple keys from input request
// and return a json formatted response to signal success / failure
func CacheDelete(w http.ResponseWriter, r *http.Request) {
	HitCount = HitCount + 1
	keys := r.URL.Query()["key"]
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	var results CacheResponses
	var result CacheResponse
	for _, k := range keys {
		result.Key = k
		result.Status = false
		if Data.Delete(k) {
			result.Status = true
		}
		results.Response = append(results.Response, result)
	}
	results.Operation = constants.DELETE_OPERATION
	results.TimeStamp = time.Now().UTC()
	byteData, err := json.MarshalIndent(results, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)
}

// CacheFlush will drop all data from the cache and return
// a json formatted response to signal success / failure
func CacheFlush(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	if GetENValueWithDefault(constants.DEV_TEST, constants.FalseValue) == constants.FalseValue {
		Data.DeleteAll()
	}
	var results CacheResponses
	results.Operation = constants.FLUSH_OPERATION
	results.TimeStamp = time.Now().UTC()
	byteData, err := json.MarshalIndent(results, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)

}

// CacheDump will return all data from the cache in
// a json formatted response.
func CacheDump(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	var results CacheResponses
	results.Operation = constants.DUMP_OPERATION
	if GetENValueWithDefault(constants.DEV_TEST, constants.FalseValue) == constants.FalseValue {
		results.DBData = Data.GetAll()
	} else {
		dbDump := make(map[string]interface{})
		results.DBData = dbDump
	}
	results.TimeStamp = time.Now().UTC()
	byteData, err := json.MarshalIndent(results, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)

}

// CacheDump will return metrics data from the cache in
// a json formatted response. The metrics data are as follows
// Total number of input keys
// Total size of values stored in-memory in bytes
// Total numer of /Set/Get/Delete operations
func CacheMetrics(w http.ResponseWriter, r *http.Request) {
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)
	metrics_data := Metrics{}
	metrics_data.HitCount = HitCount
	if GetENValueWithDefault(constants.DEV_TEST, constants.FalseValue) == constants.FalseValue {
		metrics_data.KeyCount, metrics_data.Size = Data.GetMetrics()
	} else {
		metrics_data.KeyCount, metrics_data.Size = rand.Intn(100), rand.Intn(5000)
	}

	byteData, err := json.MarshalIndent(metrics_data, "", " ")
	if err != nil {
		fmt.Printf("json marshall error %v\n", zap.Error(err))
		fmt.Fprintln(w, err)
	}
	w.Write(byteData)
}
