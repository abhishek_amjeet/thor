package router

import (
	"net/http"
	"time"
)

// The models here are bascially REST api constructs
// They will be used as the standard format across the API server
// and the CLI tool

type Route struct {
	Name        string           `json:"name"`
	Method      string           `json:"method"`
	Pattern     string           `json:"pattern"`
	HandlerFunc http.HandlerFunc `json:"handlerFunc"`
}

type Routes []Route

type Metrics struct {
	KeyCount int `json:"key_count"`
	Size     int `json:"data_size"`
	HitCount int `json:"hit_count"`
}

type CacheResponse struct {
	Key    string `json:"key"`
	Value  string `json:"value"`
	Status bool   `json:"status"`
}

type CacheResponses struct {
	Response  []CacheResponse        `json:"response"`
	Operation string                 `json:"operation"`
	DBData    map[string]interface{} `json:"dbDump"`
	TimeStamp time.Time              `json:"timeStamp"`
}
