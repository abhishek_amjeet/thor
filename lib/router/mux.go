package router

import (
	"context"
	"fmt"
	"github.com/gorilla/mux"
	"go.uber.org/zap"
	"net/http"
	"os"
	"os/signal"
	"thor/lib/cache"
	"thor/lib/constants"
	"thor/lib/klog"
	"thor/lib/middleware"
	"time"
)

var Data cache.MemCache

// StartMux starts the gorilla mux
// this implements the go serve mux
func StartMux(ctx context.Context) {
	Data = cache.New()

	log := klog.Logger(ctx)
	routes := GetRoutes()
	router := mux.NewRouter().StrictSlash(true)

	for _, route := range *routes {
		var handler http.Handler
		handler = route.HandlerFunc
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)

	}
	router.Use(middleware.LoggingMiddleware)

	server_url := fmt.Sprintf("%s:%s", constants.SERVER_ADDRESS, GetPortDefault())
	log.Sugar().Infof("Starting web server at http://%s", server_url)
	srv := &http.Server{
		Addr:         server_url,
		WriteTimeout: time.Second * 15,
		ReadTimeout:  time.Second * 15,
		IdleTimeout:  time.Second * 60,
		Handler:      router, // Pass our instance of gorilla/mux in.
	}

	go func() {
		if err := srv.ListenAndServe(); err != nil {
			log.Sugar().Warn(err)
		}
	}()

	// walk all routes
	err := router.Walk(func(route *mux.Route, router *mux.Router, ancestors []*mux.Route) error {
		api, err := route.GetPathTemplate()
		if err != nil {
			log.Sugar().Error("Unable to get path templates %v", zap.Error(err))
			return err
		}
		methods, err := route.GetMethods()
		if err != nil {
			log.Sugar().Error("Unable to get methods %v", zap.Error(err))
			return err
		}
		fmt.Printf("Name == > %s : Route ==> %v: Methods = %v\n", route.GetName(), api, methods)
		return nil
	})
	if err != nil {
		log.Sugar().Error("Unable to walk routes %v", zap.Error(err))
		panic(err)
	}

	// notify before shutdown , graceful shutdown
	c := make(chan os.Signal, 1)
	signal.Notify(c, os.Interrupt)
	// Block until we receive our signal.
	<-c
	srv.Shutdown(ctx)
	log.Sugar().Info("shutting down")
	os.Exit(0)
}
