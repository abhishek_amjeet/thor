package router

//GetRoutes populates all the routes that are needed
func GetRoutes() *Routes {
	return &Routes{
		Route{
			"Health Endpoint",
			"GET",
			"/health",
			Health,
		},
		Route{
			"Get Key",
			"GET",
			"/Get",
			CacheGet,
		},
		Route{
			"Set Key",
			"GET",
			"/Set",
			CacheInsert,
		},
		Route{
			"Delete Key",
			"GET",
			"/Delete",
			CacheDelete,
		},
		Route{
			"Delete Cache",
			"GET",
			"/Flush",
			CacheFlush,
		},
		Route{
			"Memory Dump",
			"GET",
			"/Dump",
			CacheDump,
		},
		Route{
			"System stats",
			"GET",
			"/Metrics",
			CacheMetrics,
		},
	}
}
