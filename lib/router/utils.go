package router

import (
	"os"
	"thor/lib/constants"
)

// helper utility to fetch PORT number from env variables
func GetPortDefault() string {
	port, ok := os.LookupEnv(constants.WEB_PORT_KEY)
	if ok {
		return port
	}
	return constants.DEFAULT_PORT_NUM
}

// helper utility to fetch ENV variiable with default
func GetENValueWithDefault(envKey, defaultValue string) string {
	value, ok := os.LookupEnv(envKey)
	if ok {
		return value
	}
	return defaultValue
}
