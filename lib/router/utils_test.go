package router

import (
	"github.com/stretchr/testify/assert"
	"os"
	"testing"
	"thor/lib/constants"
)

// TestGetPort tests the get port utility
func TestGetPort(t *testing.T) {
	os.Setenv(constants.WEB_PORT_KEY, "9999")
	portnum := GetPortDefault()
	assert.Equal(t, portnum, "9999")

	os.Unsetenv(constants.WEB_PORT_KEY)
	portnum = GetPortDefault()
	assert.Equal(t, portnum, constants.DEFAULT_PORT_NUM)
}

// TestGetEnvValueWithDefault tests the get env value utility
func TestGetEnvValueWithDefault(t *testing.T) {
	os.Setenv("TEST_KEY", "9999")
	envValue := GetENValueWithDefault("TEST_KEY", "9999")
	assert.Equal(t, envValue, "9999")

	os.Unsetenv("TEST_KEY")
	envValue = GetENValueWithDefault("TEST_KEY", "77777")
	assert.Equal(t, envValue, "77777")
}
