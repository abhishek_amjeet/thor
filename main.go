package main

import (
	"context"
	"flag"
	"thor/lib/router"
	"time"
)

func main() {
	// the context timeout for each http call
	wait := time.Second * 30
	flag.DurationVar(&wait, "graceful-timeout", wait, "The duration for which the server will wait gracefully")
	flag.Parse()
	ctx, cancel := context.WithTimeout(context.Background(), wait)
	defer cancel()
	router.StartMux(ctx)
}
